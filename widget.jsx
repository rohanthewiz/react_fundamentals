import React from 'react';

// WIDGET
export default class Widget extends React.Component {
  render() {
    return (
      <div>
        {/* bind the input's onChange to the provided update method */}
        <input type="text" onChange={ this.props.update } placeholder="Type in here"/>
        <h2>{ this.props.txt }</h2>
      </div>
    );
  }
}
