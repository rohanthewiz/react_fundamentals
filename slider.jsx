import React from 'react';

// SLIDER
export default class Slider extends React.Component {
  constructor() {
    super();
    this.state = { val: 0 };
  }

  update(e) {
    this.setState({val: e.target.value});
    this.props.update(e); // Let the parent know
  }

  render() {
    return (
      <div>
        <span>{this.props.name}</span>  <span>[{this.state.val}]</span>

        {/* bind the input's onChange to our local handler */}
        <input type="range"
               min="0"
               max="255"
               value={this.state.val}
               onChange={ this.update.bind(this) }
               style={{'margin-left': '1em'}}
        />
      </div>
    );
  }
}
