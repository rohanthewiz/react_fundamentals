import React from 'react';
import ReactDOM from 'react-dom';
import Widget from './widget.jsx';
import Slider from './slider.jsx';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      txt: 'txt - Default state',
      red_val: 0,
      green_val: 0,
      blue_val: 0
    };
  }

  // Update state just before component mounted
  componentWillMount() {
    this.setState({txt: this.props.txt});
  }

  update(e) {
    // We are only concerned with the 'txt' state here
    this.setState({txt: e.target.value});
  }
  
  update_red(e) {
    this.setState({red_val: e.target.value})
  }
  update_green(e) {
    this.setState({green_val: e.target.value})
  }
  update_blue(e) {
    this.setState({blue_val: e.target.value})
  }

  render() {
    return (
      <div style={{'background-color':'wheat','padding':'1em'}}>
        <h1>A Reactjs Exercise</h1>
        {/* Any child component can update the parent, via the passed in handler
         * However the resulting change to the parents state will be trickled down
         * to the children since their 'txt' prop is set from the parents 'txt' state */}
        <Widget
          txt={this.state.txt} // 'this' here still refers to App
          update={this.update.bind(this)} />
        <div>RGB({this.state.red_val}, {this.state.green_val}, {this.state.blue_val})</div>
        <hr />
        <Slider name="Red" update={this.update_red.bind(this)} />
        <Slider name="Green" update={this.update_green.bind(this)}/>
        <Slider name="Blue" update={this.update_blue.bind(this)}/>
      </div>
    );
  }
}

App.propTypes = {
  txt: React.PropTypes.string.isRequired // Optional isRequired - property must be included
};

App.defaultProps = {
  txt: "This is the default prop 'txt'" // The default is used if no txt attribute is passed into <App />
};

ReactDOM.render(
  <App txt="This is the passed in props 'txt'" />,
  document.getElementById('app')
);
