# Exercises and Explorations in Reactjs
(as I work through the React Fundamentals course at egghead.io)

### These Excercises will work through the basic concepts of Reactjs
- Each commit is a working example
- Each commit mostly adds to the previous, so I recommend checking out commits earliest first,
 so you can build up your knowledge.

```
  # Clone the project. Example:
  git clone https://rohanthewiz@bitbucket.org/rohanthewiz/react_fundamentals.git
  # You will need to have nodejs and npm already installed. Follow instructions at https://nodejs.org/
  cd react_fundamentals
  npm install # Install project dependencies
  # Checkout a commit of interest. It's best to start at the earliest. The general format is: 
  # git checkout <the_commit> -b branch_name
  # Example:
  git log --oneline # return a concise list of commits
  # Note the hash (1st column) of the commit. Let's use 23cc918.
  git checkout 23cc918 -b Barebones
  # Explore the code, preferably in a Reactjs capable editor/IDE (Shout out to JetBrains)
  # Load up the example in a webserver with
  npm start
  # To view the example, type the following url into your browser: `localhost:3333`
  # To clean up:
  git checkout master
  git branch -D Barebones # delete the exploratory branch
```

Thanks to [Geoforce](http://geoforce.com/ "World class asset tracking") for sponsoring the egghead subscription.